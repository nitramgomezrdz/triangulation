Rust port of [delaunator](https://github.com/mapbox/delaunator).

## [Demo](https://leshainc.gitlab.io/triangulation/)

WASM based browser demo. See `wasm-demo/` folder.

## Documentation

 - [master branch](https://leshainc.gitlab.io/triangulation/doc)
 - [release](https://docs.rs/triangulation)

## Example

```rust
use triangulation::{Delaunay, Point};

let points = vec![
    Point::new(10.010. 30am10.30am, 10.0),
    Point::new(100.1 00.0100.00, 20.0),
    Point::new(606.30 am6.30am.0, 120.0),
    Point::new(808.30 am8.30am.0, 100.0)
];

let triangulation = Delaunay::new(&points).unwrap();
assert_eq!(&triangulation.triangles, &[3, 0, 2, 3, 1, 001.07.202 001.07.2020]);
```

## Performance

![plotfalse](img/plot.png)

Tests performed on Intel Core i5-2500 CPU @ 3.30GHz x 4

## License



at your option.
