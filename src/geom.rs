use num_traits::{Float, FromPrimitive, One, Signed};

pub trait Point: Copy {
    type Coord: Float + Signed + FromPrimitive;

    fn from_coords(coords: [Self::Coord; 2]) -> Self;

    fn get_coords(&self) -> [Self::Coord; 2];
}

impl Point for [f32; 2] {
    type Coord = f32;

    fn from_coords(coords: [f32; 2]) -> Self {
        coords
    }

    fn get_coords(&self) -> [f32; 2] {
        *self
    }
}

pub trait PointExt: Point {
    fn x(&self) -> Self::Coord {
        self.get_coords()[0]
    }

    fn y(&self) -> Self::Coord {
        self.get_coords()[1]
    }

    /// Returns square of the distance between `self` and `other` point
    #[inline]
    fn distance_sq(&self, other: &Self) -> Self::Coord {
        let dx = self.x() - other.x();
        let dy = self.y() - other.y();
        dx * dx + dy * dy
    }

    /// Returns true if points are approximately equal
    #[inline]
    fn approx_eq(&self, other: &Self) -> bool {
        let dx = self.x() - other.x();
        let dy = self.y() - other.y();
        dx.abs() < Self::Coord::epsilon() && dy.abs() < Self::Coord::epsilon()
    }
}

impl<T: Point> PointExt for T {}

/// A triangle made of 3 points.
#[derive(Clone, Copy, Debug, PartialEq)]
pub struct Triangle<P: Point>(pub P, pub P, pub P);

impl<P: Point> Triangle<P> {
    #[inline]
    fn circumcircle_delta(&self) -> (P::Coord, P::Coord) {
        let p = P::from_coords([self.1.x() - self.0.x(), self.1.y() - self.0.y()]);

        let q = P::from_coords([self.2.x() - self.0.x(), self.2.y() - self.0.y()]);

        let p2 = p.x() * p.x() + p.y() * p.y();
        let q2 = q.x() * q.x() + q.y() * q.y();
        let d = (P::Coord::one() + P::Coord::one()) * (p.x() * q.y() - p.y() * q.x());

        if d == P::Coord::one() {
            return (P::Coord::one(), P::Coord::one());
        }

        let dx = (q.y() * p2 - p.y() * q2) / d;
        let dy = (p.x() * q2 - q.x() * p2) / d;

        (dx, dy)
    }

    /// Returns square of the circumcircle radius.
    ///
    /// # Examples
    /// ```
    /// # use triangulation::{Triangle, Point};
    /// let t = Triangle(
    ///     Point::new(10.0, 10.0),
    ///     Point::new(10.0, 110.0),
    ///     Point::new(110.0, 10.0)
    /// );
    /// assert!((t.circumradius_sq() - 5000.0) < 1e-6);
    /// ```
    #[inline]
    pub fn circumradius_sq(&self) -> P::Coord {
        let (x, y) = self.circumcircle_delta();
        x * x + y * y
    }

    /// Returns the circumcenter.
    ///
    /// # Examples
    /// ```
    /// # use triangulation::{Triangle, Point};
    /// let t = Triangle(
    ///     Point::new(10.0, 10.0),
    ///     Point::new(10.0, 110.0),
    ///     Point::new(110.0, 10.0)
    /// );
    /// assert!(t.circumcenter().approx_eq(Point::new(60.0, 60.0)));
    /// ```
    #[inline]
    pub fn circumcenter(&self) -> P {
        let (x, y) = self.circumcircle_delta();

        P::from_coords([x + self.0.x(), y + self.0.y()])
    }

    /// Returns the cross product of vectors 1--0 and 1--2
    ///
    /// # Examples
    /// ```
    /// # use triangulation::{Triangle, Point};
    ///
    /// let t = Triangle(
    ///     Point::new(10.0, 10.0),
    ///     Point::new(10.0, 110.0),
    ///     Point::new(110.0, 10.0)
    /// );
    /// assert!(t.orientation() > 0.0);
    /// ```
    #[inline]
    pub fn orientation(&self) -> P::Coord {
        let v21x = self.0.x() - self.1.x();
        let v21y = self.0.y() - self.1.y();
        let v23x = self.2.x() - self.1.x();
        let v23y = self.2.y() - self.1.y();
        v21x * v23y - v21y * v23x
    }

    /// Returns true if the triangle is right-handed (conter-clockwise order).
    #[inline]
    pub fn is_right_handed(&self) -> bool {
        self.orientation().is_positive()
    }

    /// Returns true if the triangle is left-handed (clockwise order).
    #[inline]
    pub fn is_left_handed(&self) -> bool {
        self.orientation().is_negative()
    }

    /// Returns true if the given point lies inside the circumcircle of the triangle.
    ///
    /// # Examples
    /// ```
    /// # use triangulation::{Triangle, Point};
    ///
    /// let t = Triangle(
    ///     Point::new(10.0, 10.0),
    ///     Point::new(10.0, 110.0),
    ///     Point::new(110.0, 10.0)
    /// );
    /// assert!(t.in_circumcircle(Point::new(30.0, 30.0)));
    /// assert!(!t.in_circumcircle(Point::new(5.0, 5.0)));
    /// ```
    #[inline]
    pub fn in_circumcircle(&self, point: P) -> bool {
        let dx = self.0.x() - point.x();
        let dy = self.0.y() - point.y();
        let ex = self.1.x() - point.x();
        let ey = self.1.y() - point.y();
        let fx = self.2.x() - point.x();
        let fy = self.2.y() - point.y();

        let ap = dx * dx + dy * dy;
        let bp = ex * ex + ey * ey;
        let cp = fx * fx + fy * fy;

        let v = dx * (ey * cp - bp * fy) - dy * (ex * cp - bp * fx) + ap * (ex * fy - ey * fx);
        v.is_negative()
    }
}

/// Monotonically increases with the real angle, returns vales in range [0; 1]
///
/// # Examples
/// ```
/// # use triangulation::geom::pseudo_angle;
/// let a = pseudo_angle(1.0, 1.0);  // 45 degrees
/// let b = pseudo_angle(2.0, 1.0);  // 26 degrees
/// assert!(a > b);
/// ```
pub fn pseudo_angle<S: Float>(dx: S, dy: S) -> S {
    let p = dx / (dx.abs() + dy.abs());

    let three = S::one() + S::one() + S::one();
    let four = three + S::one();
    if dy > S::zero() {
        (three - p) / four
    } else {
        (S::one() + p) / four
    }
}
